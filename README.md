### Radius proxy

[![N|Solid](https://www.juniper.net/assets/img/logos/juniper/juniper-networks-white-s.png)](https://www.juniper.net/us/en/)

#### What problem does the radius proxy application try to solve here?
Some customers using E-series BNG platform and developed/deployed the radius backend system 
using the NAS-PORT-ID attribute as a key to authenticate incoming call want MX-series platform 
to generate a compatible NAS-PORT-ID format as like the E-series platform's NAS-PORT-ID.
Today, MX-platform and JUNOS software does not support the programming capability of the zero-filled sub-interface unit index value 
reflecting VLAN values (S/C-tags) of subscriber access-line or E-series compatible NAS-PORT-ID format. 
So the custom radius proxy application works as a middle-man to translate incoming NAS-PORT-ID 
from MX-series into new NAS-PORT-ID in E-series format.


![Radius proxy role](radius_proxy_role.jpg)

#### Input arguments format
The radius proxy application supports two options.
1. -t : timeout value for customer's radius server response. Default value is 3 seconds.
2. -m : radius proxy mapping entries. multiple entries can be specified with comma separator (',')
>   \<local ip>:\<local port>-<remote ip>:\<remote port>-\<radius secret>, ...
   
  The number of mapping entry is unlimited.
  You can copy jrp.py and rename it and use it if you want to have application redundancy.
  For instance, 
  > jrp1.py for 1.1.1.1:11812-100.0.0.1:1812-juniper<br/>
  > jrp2.py for 1.1.1.1:11813-100.0.0.1:1813-juniper<br/>
  > jrp3.py for 1.1.1.1:21812-100.0.0.1:1812-juniper<br/>
  > jrp4.py for 1.1.1.1:21813-100.0.0.1:1813-juniper
  
  The radius secret key can be a <strong>plain string</strong> or <strong>JUNOS encrypted format</strong> value (starting with $9$ prefix)
  You can copy JUNOS encrypted secret key from radius-server configuration on Junos configuration mode CLI
 
#### installation steps
1. copy jrp.py into MX BNG system.
2. register jrp.py file into JET application with radius proxy mapping arguments.
3. add/update JUNOS radius client configuration pointing new radius proxy server listen ip and port. 

```sh
$ scp jrp.py user@bng1:
$ ssh user@bng1
Warning: Permanently added 'petrel1.englab.juniper.net,10.9.46.227' (ECDSA) to the list of known hosts.
Last login: Fri Oct  2 06:04:55 2020 from 10.107.13.117
--- JUNOS 20.2R1.10 Kernel 64-bit  JNPR-11.0-20200608.0016468_buil
user@bng1> 
user@bng1> request system scripts refresh-from extension-service file jrp.py url /var/home/user/ 
refreshing 'jrp.py' from '/var/home/user/jrp.py'
user@bng1> 
user@bng1> file list /var/db/scripts/jet/jrp.py detail 
-rw-r-----  1 root  wheel       9283 Oct 2  16:48 /var/db/scripts/jet/jrp.py
total files: 1

user@bng1>
user@bng1> edit 
Entering configuration mode
The configuration has been changed but not committed

[edit]
user@bng1# show system 
scripts {
    language python3;
}
extensions {
    providers {
        jnpr {
            license-type juniper deployment-scope commercial;
        }
    }
    extension-service {
        application {
            file jrp.py {
                arguments "-m 1.1.1.1:1812-100.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-100.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj";
                daemonize;
                respawn-on-normal-exit;
                username root;
            }
        }
    }
}

[edit]
user@bng1# show interfaces lo0 
unit 0 {
    family inet {
        address 1.1.1.1/32;
    }
}

[edit]
user@bng1# show access radius-server 
1.1.1.1 {
    port 1812;
    accounting-port 1813;
    secret "$9$JfUi.QF/0BEP5BEcyW8ZUj"; ## SECRET-DATA
    timeout 1;
    retry 3;
    max-outstanding-requests 500;
    source-address 1.1.1.1;
}

[edit]
user@bng1# show access profile freeradius 
authentication-order radius;
radius {
    authentication-server 1.1.1.1;
    accounting-server 1.1.1.1;
}
accounting {
    order radius;
}

[edit]

```


#### radius message support matrix
| code                             | supported? |
|----------------------------------|------------|
| access-request                   | yes        |
| accounting-request start         | yes        |
| accounting-interim start         | yes        |
| coa request                      | no         |
| disconnect request               | no         |

#### NAS-PORT-ID translation result example
| MX NAS-PORT-ID                                    |  Radius Proxy NAS-PORT-ID |
| ------------------------------------------------- | ------------------------- |
| ps0.100:100                                       | ps0.00000100:0-100        |
| :100                                              | .00000100:0-100           |
| 100-200                                           | .01000200:100-200         |
| ge-2/0/0.demux0.3221225498:100-200:100-200        | ge-2/0/0.01000200:100-200 |
| ge-2/0:100-200                                    | ge-2/0.01000200:100-200   |
| ge-2/0/0.demux0.3221225506:100-200#ge-2/0:100-200 | ge-2/0/0.01000200:100-200 |
| ge-2/0/0:100-200:100-200                          | ge-2/0/0.01000200:100-200 |
| ge-2/0/0:100-200                                  | ge-2/0/0.01000200:100-200 |
| ge-2/0/0.demux0.3221225512:100-200                | ge-2/0/0.01000200:100-200 |
| ae100.demux0.3221225512:100-200                   | ae100.01000200:100-200    |

#### test subscriber session
```sh
user@bng1> show subscribers 
Interface             IP Address/VLAN ID                      User Name                      LS:RI
demux0.3221225731     0x8100.100 0x8100.200                                             default:default      
pp0.3221225732        203.0.0.56                              jackson                   default:default      

user@bng1> show subscribers extensive 
Type: VLAN
Logical System: default
Routing Instance: default
Interface: demux0.3221225731
Interface type: Dynamic
Underlying Interface: ge-2/0/0
Dynamic Profile Name: vlan-profile1
State: Active
Session ID: 260
PFE Flow ID: 489
Stacked VLAN Id: 0x8100.100
VLAN Id: 0x8100.200
Login Time: 2020-10-02 18:09:50 UTC

Type: PPPoE
User Name: jackson
IP Address: 203.0.0.56
IP Netmask: 255.255.255.255
Primary DNS Address: 8.8.8.8
Logical System: default
Routing Instance: default
Interface: pp0.3221225732
Interface type: Dynamic
Underlying Interface: demux0.3221225731
Dynamic Profile Name: pppoe-profile1
MAC Address: 08:00:27:a2:0b:1c
State: Active
Radius Accounting ID: 261
Session ID: 261
PFE Flow ID: 491
Stacked VLAN Id: 100
VLAN Id: 200
Login Time: 2020-10-02 18:09:50 UTC
IP Address Pool: p1
Accounting interval: 600

```

#### radius proxy log message on JUNOS system
```syslog
% tail -f /var/log/jrp.log
2020-10-02 18:08:13.458 INFO 264: **********************
2020-10-02 18:08:13.459 INFO 265: radius proxy starts!!!
2020-10-02 18:08:13.459 INFO 266: **********************
2020-10-02 18:08:13.459 DEBUG 267: arguments: ['/var/run/scripts/jet//jrp.py', '-m', '1.1.1.1:1812-100.0.0.1:1812-$9$JfUi.QF/0BEP5BEcyW8ZUj,1.1.1.1:1813-100.0.0.1:1813-$9$JfUi.QF/0BEP5BEcyW8ZUj']
2020-10-02 18:08:13.462 DEBUG 285: proxy mapping: 1.1.1.1:1812 <==> 100.0.0.1:1812 : $9$JfUi.QF/0BEP5BEcyW8ZUj
2020-10-02 18:08:13.464 DEBUG 285: proxy mapping: 1.1.1.1:1813 <==> 100.0.0.1:1813 : $9$JfUi.QF/0BEP5BEcyW8ZUj
2020-10-02 18:08:21.762 DEBUG 173: code: access-request     nas-port-id translate: ge-2/0/0.demux0.3221225729:100-200 -> ge-2/0/0.01000200:100-200
2020-10-02 18:08:21.963 DEBUG 173: code: accounting-request nas-port-id translate: ge-2/0/0.demux0.3221225729:100-200 -> ge-2/0/0.01000200:100-200
2020-10-02 18:09:04.962 DEBUG 173: code: accounting-request nas-port-id translate: ge-2/0/0.demux0.3221225729:100-200 -> ge-2/0/0.01000200:100-200
2020-10-02 18:09:50.462 DEBUG 173: code: access-request     nas-port-id translate: ge-2/0/0.demux0.3221225731:100-200 -> ge-2/0/0.01000200:100-200
2020-10-02 18:09:50.662 DEBUG 173: code: accounting-request nas-port-id translate: ge-2/0/0.demux0.3221225731:100-200 -> ge-2/0/0.01000200:100-200
```

#### freeradius server accounting data
```sh
Fri Oct  2 14:11:10 2020
	User-Name = "jackson"
	Acct-Status-Type = Start
	Acct-Session-Id = "261"
	Event-Timestamp = "Oct  2 2020 14:09:51 EDT"
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	ERX-Attr-177 = 0x506f72742073706565643a20313030303030306b
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "0800.27a2.0b1c"
	Framed-IP-Address = 203.0.0.56
	Framed-IP-Netmask = 255.255.255.255
	NAS-Identifier = "petrel1"
	NAS-Port = 536871112
	NAS-Port-Id = "ge-2/0/0.01000200:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "pppoe 08:00:27:a2:0b:1c"
	ERX-Attr-210 = 0x00000004
	NAS-IP-Address = 1.1.1.1
	Acct-Unique-Session-Id = "2a40db6dede85297"
	Timestamp = 1601662270

Fri Oct  2 14:21:10 2020
	User-Name = "jackson"
	Acct-Status-Type = Interim-Update
	Acct-Session-Id = "261"
	Event-Timestamp = "Oct  2 2020 14:19:50 EDT"
	Acct-Session-Time = 600
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	ERX-Attr-177 = 0x506f72742073706565643a20313030303030306b
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "0800.27a2.0b1c"
	Framed-IP-Address = 203.0.0.56
	Framed-IP-Netmask = 255.255.255.255
	NAS-Identifier = "petrel1"
	NAS-Port = 536871112
	NAS-Port-Id = "ge-2/0/0.01000200:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "pppoe 08:00:27:a2:0b:1c"
	ERX-Attr-210 = 0x00000002
	NAS-IP-Address = 1.1.1.1
	Acct-Unique-Session-Id = "2a40db6dede85297"
	Timestamp = 1601662870

Fri Oct  2 14:21:36 2020
	User-Name = "jackson"
	Acct-Status-Type = Stop
	Acct-Session-Id = "261"
	Event-Timestamp = "Oct  2 2020 14:20:17 EDT"
	Acct-Session-Time = 626
	Acct-Terminate-Cause = User-Request
	Acct-Delay-Time = 0
	Service-Type = Framed-User
	Framed-Protocol = PPP
	Acct-Authentic = RADIUS
	ERX-Dhcp-Mac-Addr = "0800.27a2.0b1c"
	Framed-IP-Address = 203.0.0.56
	Framed-IP-Netmask = 255.255.255.255
	NAS-Identifier = "petrel1"
	NAS-Port = 536871112
	NAS-Port-Id = "ge-2/0/0.01000200:100-200"
	NAS-Port-Type = Ethernet
	ERX-Virtual-Router-Name = "default:default"
	ERX-Pppoe-Description = "pppoe 08:00:27:a2:0b:1c"
	NAS-IP-Address = 1.1.1.1
	Acct-Unique-Session-Id = "2a40db6dede85297"
	Timestamp = 1601662896

```

#### software version
##### JUNOS
```sh
user@bng1# run show version 
Hostname: bng1
Model: mx480
Junos: 20.2R1.10
JUNOS OS Kernel 64-bit  [20200608.0016468_builder_stable_11]
JUNOS OS libs [20200608.0016468_builder_stable_11]
JUNOS OS runtime [20200608.0016468_builder_stable_11]
JUNOS OS time zone information [20200608.0016468_builder_stable_11]
JUNOS network stack and utilities [20200625.123713_builder_junos_202_r1]
JUNOS libs [20200625.123713_builder_junos_202_r1]
JUNOS OS libs compat32 [20200608.0016468_builder_stable_11]
JUNOS OS 32-bit compatibility [20200608.0016468_builder_stable_11]
JUNOS libs compat32 [20200625.123713_builder_junos_202_r1]
JUNOS runtime [20200625.123713_builder_junos_202_r1]
JUNOS sflow mx [20200625.123713_builder_junos_202_r1]
JUNOS py extensions2 [20200625.123713_builder_junos_202_r1]
JUNOS py extensions [20200625.123713_builder_junos_202_r1]
JUNOS py base2 [20200625.123713_builder_junos_202_r1]
JUNOS py base [20200625.123713_builder_junos_202_r1]
JUNOS OS crypto [20200608.0016468_builder_stable_11]
JUNOS na telemetry [20.2R1.10]
JUNOS Security Intelligence [20200625.123713_builder_junos_202_r1]
JUNOS mx libs compat32 [20200625.123713_builder_junos_202_r1]
JUNOS mx runtime [20200625.123713_builder_junos_202_r1]
JUNOS RPD Telemetry Application [20.2R1.10]
Redis [20200625.123713_builder_junos_202_r1]
JUNOS common platform support [20200625.123713_builder_junos_202_r1]
JUNOS Openconfig [20.2R1.10]
JUNOS mtx network modules [20200625.123713_builder_junos_202_r1]
JUNOS modules [20200625.123713_builder_junos_202_r1]
JUNOS mx modules [20200625.123713_builder_junos_202_r1]
JUNOS mx libs [20200625.123713_builder_junos_202_r1]
JUNOS SQL Sync Daemon [20200625.123713_builder_junos_202_r1]
JUNOS mtx Data Plane Crypto Support [20200625.123713_builder_junos_202_r1]
JUNOS daemons [20200625.123713_builder_junos_202_r1]
JUNOS mx daemons [20200625.123713_builder_junos_202_r1]
JUNOS appidd-mx application-identification daemon [20200625.123713_builder_junos_202_r1]
JUNOS Services URL Filter package [20200625.123713_builder_junos_202_r1]
JUNOS Services TLB Service PIC package [20200625.123713_builder_junos_202_r1]
JUNOS Services Telemetry [20200625.123713_builder_junos_202_r1]
JUNOS Services TCP-LOG [20200625.123713_builder_junos_202_r1]
JUNOS Services SSL [20200625.123713_builder_junos_202_r1]
JUNOS Services SOFTWIRE [20200625.123713_builder_junos_202_r1]
JUNOS Services Stateful Firewall [20200625.123713_builder_junos_202_r1]
JUNOS Services RTCOM [20200625.123713_builder_junos_202_r1]
JUNOS Services RPM [20200625.123713_builder_junos_202_r1]
JUNOS Services PCEF package [20200625.123713_builder_junos_202_r1]
JUNOS Services NAT [20200625.123713_builder_junos_202_r1]
JUNOS Services Mobile Subscriber Service Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services MobileNext Software package [20200625.123713_builder_junos_202_r1]
JUNOS Services Logging Report Framework package [20200625.123713_builder_junos_202_r1]
JUNOS Services LL-PDF Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services Jflow Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services Deep Packet Inspection package [20200625.123713_builder_junos_202_r1]
JUNOS Services IPSec [20200625.123713_builder_junos_202_r1]
JUNOS Services IDS [20200625.123713_builder_junos_202_r1]
JUNOS IDP Services [20200625.123713_builder_junos_202_r1]
JUNOS Services HTTP Content Management package [20200625.123713_builder_junos_202_r1]
JUNOS Services Crypto [20200625.123713_builder_junos_202_r1]
JUNOS Services Captive Portal and Content Delivery Container package [20200625.123713_builder_junos_202_r1]
JUNOS Services COS [20200625.123713_builder_junos_202_r1]
JUNOS AppId Services [20200625.123713_builder_junos_202_r1]
JUNOS Services Application Level Gateways [20200625.123713_builder_junos_202_r1]
JUNOS Services AACL Container package [20200625.123713_builder_junos_202_r1]
JUNOS SDN Software Suite [20200625.123713_builder_junos_202_r1]
JUNOS Extension Toolkit [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (wrlinux9) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (MXSPC3) [20.2R1.10]
JUNOS Packet Forwarding Engine Support (MX/EX92XX Common) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (M/T Common) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (aft) [20200625.123713_builder_junos_202_r1]
JUNOS Packet Forwarding Engine Support (MX Common) [20200625.123713_builder_junos_202_r1]
JUNOS Juniper Malware Removal Tool (JMRT) [1.0.0+20200625.123713_builder_junos_202_r1]
JUNOS J-Insight [20200625.123713_builder_junos_202_r1]
JUNOS jfirmware [20200625.123713_builder_junos_202_r1]
JUNOS Online Documentation [20200625.123713_builder_junos_202_r1]
JUNOS jail runtime [20200608.0016468_builder_stable_11]

```

##### Python
```sh
python3.7
```